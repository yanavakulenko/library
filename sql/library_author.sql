
DROP TABLE IF EXISTS `author`;


CREATE TABLE `author` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `dob` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `firstname` (`firstname`,`lastname`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
