DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `bookname` varchar(30) NOT NULL,
  `created` date DEFAULT NULL,
  `isbn` varchar(20) DEFAULT NULL,
  `author_id` int(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id_idx` (`author_id`),
  CONSTRAINT `author_id` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
