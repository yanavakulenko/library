package com.dataart.library.service;

import java.util.List;

import com.dataart.library.model.Book;

public interface BookService {
	
	void save(Book book);

	void delete(Book id);

	Book findOne(Integer id);

	List<Book> findAll();
	
	List<Book> findAll(Integer id);
	
	List<Book> findAll(String title);

}
