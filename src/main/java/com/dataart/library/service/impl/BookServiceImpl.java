package com.dataart.library.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dataart.library.dao.BookDAO;
import com.dataart.library.model.Book;
import com.dataart.library.service.BookService;

@Service
@Transactional
public class BookServiceImpl implements BookService {

	@Autowired
	private BookDAO dao;

	@Override
	public void save(Book book) {
		dao.save(book);
	}

	@Override
	public void delete(Book book) {
		dao.delete(book);
	}

	@Override
	public Book findOne(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public List<Book> findAll() {
		return dao.findAll();
	}

	@Override
	public List<Book> findAll(Integer id) {
		return dao.findAll(id);
	}

	@Override
	public List<Book> findAll(String title) {
		return dao.findAll(title);
	}

}
