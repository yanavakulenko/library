package com.dataart.library.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dataart.library.dao.AuthorDAO;
import com.dataart.library.model.Author;
import com.dataart.library.service.AuthorService;

@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {

	@Autowired
	private AuthorDAO dao;

	@Override
	public void save(Author author) {
		dao.save(author);
	}

	@Override
	public void delete(Author author) {
		dao.delete(author);
	}

	@Override
	public Author findOne(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public List<Author> findAll() {
		return dao.findAll();
	}

	@Override
	public List<Author> findAll(String lastName) {
		return dao.findAll(lastName);
	}

}
