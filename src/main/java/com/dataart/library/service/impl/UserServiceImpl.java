package com.dataart.library.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.dataart.library.dao.UserDAO;
import com.dataart.library.model.Role;
import com.dataart.library.model.User;
import com.dataart.library.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	UserDAO dao;

	
	@Override
	public void save(User user) {
		Role role = new Role();
		role.setId(1);
		Set<Role> roles = new HashSet<>();
		roles.add(role);
		user.setRoles(roles);
		dao.save(user);
	}

	@Override
	public void delete(User user) {
		dao.delete(user);
	}

	@Override
	public User findOne(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public List<User> findAll() {
		return dao.findAll();
	}

	@Override
	public User findOne(String email) {
		return dao.findOne(email);
	}

}
