package com.dataart.library.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.apache.wicket.core.util.resource.locator.ResourceStreamLocator;
import org.apache.wicket.util.file.IResourceFinder;
import org.apache.wicket.util.resource.IResourceStream;
import org.apache.wicket.util.string.Strings;
import com.dataart.library.config.annotation.PagePath;

public class CustomResourceLocator extends ResourceStreamLocator {

	private static Set<String> paths = new HashSet<String>();

	static {
		paths.add("AuthorList");
		paths.add("BookList");
		paths.add("BookEdit");
		paths.add("AuthorEdit");
		paths.add("TemplatePage");
		paths.add("Activation");
		paths.add("LogIn");
		paths.add("Register");
		paths.add("HeaderPanel");
		paths.add("BookByAuthorPanel");
		paths.add("EmptyPanel");
	}

	private List<IResourceFinder> finders;

	public CustomResourceLocator() {
	}

	public CustomResourceLocator(final List<IResourceFinder> finders) {
		this.finders = finders;
	}

	@Override
	public IResourceStream locate(final Class<?> clazz, String path, final String style, final String variation,
			Locale locale, final String extension, final boolean strict) {

		String simpleFileName = null;
		if (paths.contains(Strings.lastPathComponent(clazz.getName(), '.'))) {
			if (clazz.getAnnotation(PagePath.class) != null) {
				String annotation = clazz.getAnnotation(PagePath.class).value();
				simpleFileName = Strings.lastPathComponent(annotation, '/') + "." + extension;
			}
		} else {
			simpleFileName = Strings.lastPathComponent(clazz.getName(), '.') + "." + extension;
		}
		IResourceStream stream = locate(clazz, simpleFileName);
		if (stream != null) {
			return stream;
		} else {
			return super.locate(clazz, path, style, variation, locale, extension, strict);
		}
	}

	@Override
	public IResourceStream locate(Class<?> clazz, String path) {

		IResourceStream resourceStream = null;
		for (IResourceFinder finder : finders) {
			resourceStream = finder.find(clazz, path);
			if (resourceStream != null) {
				return resourceStream;
			}
		}
		return super.locate(clazz, path);
	}

}
