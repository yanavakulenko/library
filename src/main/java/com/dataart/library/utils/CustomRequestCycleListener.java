package com.dataart.library.utils;

import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.request.handler.RenderPageRequestHandler;
import org.apache.wicket.request.IRequestHandler;
import org.apache.wicket.request.cycle.AbstractRequestCycleListener;
import org.apache.wicket.request.cycle.RequestCycle;

import com.dataart.library.wicket.page.ErrorPage;

public class CustomRequestCycleListener extends  AbstractRequestCycleListener {

	@Override
    public IRequestHandler onException(RequestCycle cycle, Exception ex) {
		
		return  new RenderPageRequestHandler(new PageProvider(new ErrorPage(ex.getMessage())));
    }

}
