package com.dataart.library.utils;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

public class Utils {
	
	public static Timestamp getCurrentDate() {
		Date date = new java.util.Date();
		return new Timestamp(date.getTime());
	}
	
	public static String getToken() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
	public static boolean isExpired(Timestamp created) {
		long difference = (getCurrentDate().getTime() - created.getTime());
		long oneHour = 60 * 60 * 1000L;
		difference = difference / oneHour;
		return difference > 24;
	}

}
