package com.dataart.library.dao;

import java.util.List;

import com.dataart.library.model.User;

public interface UserDAO {

	void save(User user);
	
	User findOne(Integer id);
	
	List <User> findAll();

	User findOne(String email);

	void delete(User user);
	
}
