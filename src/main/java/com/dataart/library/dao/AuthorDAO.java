package com.dataart.library.dao;

import java.util.List;

import com.dataart.library.model.Author;

public interface AuthorDAO {

	void save(Author author);

	void delete(Author author);

	Author findOne(Integer id);

	List<Author> findAll();
	
	List <Author> findAll(String lastName);

}
