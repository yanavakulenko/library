package com.dataart.library.dao.impl;

import java.util.List;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.dataart.library.dao.BookDAO;
import com.dataart.library.model.Book;

@Repository
@Transactional
public class BookDAOImpl implements BookDAO {

	private final static Logger LOGGER = LoggerFactory.getLogger("books");

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void save(Book book) {
		sessionFactory.getCurrentSession().saveOrUpdate(book);
	}

	@Override
	public void delete(Book book) {
		sessionFactory.getCurrentSession().delete(book);
	}

	@Override
	public Book findOne(Integer id) {
		return sessionFactory.getCurrentSession().get(Book.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> findAll() {

		return sessionFactory.getCurrentSession().createQuery("from Book").list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> findAll(Integer id) {

		return sessionFactory.getCurrentSession().createQuery("from Book where author_id = :id").setParameter("id", id)
				.list();
	}

	@Override
	public List<Book> findAll(String title) {
		return sessionFactory.getCurrentSession().createQuery("from Book where bookname like :title")
				.setParameter("title", "%" + title + "%").list();
	}

}
