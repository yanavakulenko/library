package com.dataart.library.dao.impl;

import java.util.List;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.dataart.library.dao.AuthorDAO;
import com.dataart.library.model.Author;

@Repository
@Transactional
public class AuthorDAOImpl implements AuthorDAO {

	private final static Logger LOGGER = LoggerFactory.getLogger("authors");

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void save(Author author) {
		sessionFactory.getCurrentSession().saveOrUpdate(author);
	}

	@Override
	public void delete(Author author) {
		sessionFactory.getCurrentSession().delete(author);
	}

	@Override
	public Author findOne(Integer id) {
		return sessionFactory.getCurrentSession().get(Author.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> findAll() {
		return sessionFactory.getCurrentSession().createQuery("from Author").list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Author> findAll(String lastName) {
		return sessionFactory.getCurrentSession().createQuery("from Author where lastname like :lastName")
				.setParameter("lastName", "%" + lastName + "%").list();
	}

}
