package com.dataart.library.dao;

import java.util.List;

import com.dataart.library.model.Book;

public interface BookDAO {
	
	void save(Book book);

	void delete(Book book);

	Book findOne(Integer id);

	List<Book> findAll();
	
	List<Book> findAll(Integer id);
	
	List<Book> findAll(String title);


}
