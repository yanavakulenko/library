package com.dataart.library.config;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.Page;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.core.util.file.WebApplicationPath;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.apache.wicket.util.file.IResourceFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wicketstuff.annotation.scan.AnnotatedMountScanner;
import com.dataart.library.config.annotation.AnnotationFinder;
import com.dataart.library.utils.CustomRequestCycleListener;
import com.dataart.library.utils.CustomResourceLocator;
import com.dataart.library.wicket.page.AuthorList;

public class WicketApplication extends WebApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(WicketApplication.class);

	private static final String COMMON_PATH = "/pages/";

	private static final String PACKAGE = "com.dataart.library.wicket.page";

	private static final String SCAN_PACKAGE = "com.dataart.library";

	@Override
	public void init() {
		super.init();
		getComponentInstantiationListeners().add(new SpringComponentInjector(this));

		List<IResourceFinder> finders = new ArrayList<>();
		List<String> pathToFiles = AnnotationFinder.findAnnotatedClasses(PACKAGE);

		Iterator<String> it = pathToFiles.iterator();
		while (it.hasNext()) {
			finders.add(new WebApplicationPath(getServletContext(), it.next()));
		}
		finders.add(new WebApplicationPath(getServletContext(), COMMON_PATH));

		CustomResourceLocator resourceStreamLocator = new CustomResourceLocator(finders);
		getResourceSettings().setResourceStreamLocator(resourceStreamLocator);

		getRequestCycleListeners().add(new CustomRequestCycleListener());
		
		new AnnotatedMountScanner().scanPackage(SCAN_PACKAGE).mount(this);
	}

	@Override
	public RuntimeConfigurationType getConfigurationType() {
		return RuntimeConfigurationType.DEVELOPMENT;
	}

	@Override
	public Class<? extends Page> getHomePage() {
		return AuthorList.class;
	}

}
