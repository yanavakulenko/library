package com.dataart.library.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import com.dataart.library.security.CustomAuthProvider;
import com.dataart.library.security.CustomAuthenticationFailureHandler;

@Configuration
@EnableWebSecurity
@PropertySource({ "classpath:jdbc.properties" })
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;
	
	@Autowired
	private CustomAuthProvider authProvider;
	
	@Autowired
	CustomAuthenticationFailureHandler authenticationFailureHandler;
	

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/css/**", "/js/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers("/login", "/register", "/activate/*")
		.permitAll()
		.antMatchers("/**")
		.hasAnyRole("USER", "ADMIN")
		.and()
		.formLogin()
		.loginPage("/login").failureHandler(authenticationFailureHandler)
		.permitAll()
		.and()
		.csrf().disable();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider);
	}

}
