package com.dataart.library.config.annotation;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

public class AnnotationFinder {

	private final static Logger LOGGER = LoggerFactory.getLogger(AnnotationFinder.class);

	public static List<String> findAnnotatedClasses(String packageName) {
		ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
		provider.addIncludeFilter(new AnnotationTypeFilter(PagePath.class));
		List<String> values = new ArrayList<>();
		for (BeanDefinition bean : provider.findCandidateComponents(packageName)) {
			try {
				Class<?> klass = Class.forName(bean.getBeanClassName());
				PagePath pagePathAnnotation = klass.getAnnotation(PagePath.class);
				String pathToFile = pagePathAnnotation.value().substring(0,
						pagePathAnnotation.value().lastIndexOf("/"));
				values.add(pathToFile);
			} catch (ClassNotFoundException e) {
				LOGGER.info("Error: " + e);
			}
		}
		return values;
	}

}
