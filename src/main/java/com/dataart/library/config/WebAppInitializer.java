package com.dataart.library.config;

import java.util.EnumSet;
import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.apache.wicket.protocol.http.WicketFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate5.support.OpenSessionInViewFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;

public class WebAppInitializer implements WebApplicationInitializer {

	private final static Logger LOGGER = LoggerFactory.getLogger(WebAppInitializer.class);

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {

		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.register(ApplicationConfiguration.class);
		context.register(SpringSecurityConfiguration.class);

		FilterRegistration.Dynamic springSecurityFilterChainReg = servletContext.addFilter("springSecurityFilterChain",
				DelegatingFilterProxy.class);
		springSecurityFilterChainReg.addMappingForUrlPatterns(EnumSet.of(DispatcherType.ERROR, DispatcherType.REQUEST),
				false, "/*");

		FilterRegistration.Dynamic sessionInViewFilter = servletContext.addFilter("OpenSessionInViewFilter",
				OpenSessionInViewFilter.class);
		sessionInViewFilter.setInitParameter("singleSession", "true");
		sessionInViewFilter.addMappingForUrlPatterns(null, false, "/*");

		FilterRegistration filter = servletContext.addFilter("wicket.library", WicketFilter.class);
		filter.setInitParameter("applicationClassName", WicketApplication.class.getName());
		filter.setInitParameter(WicketFilter.FILTER_MAPPING_PARAM, "/*");
		filter.addMappingForUrlPatterns(null, false, "/*");

		servletContext.addListener(new ContextLoaderListener(context));
	}

}