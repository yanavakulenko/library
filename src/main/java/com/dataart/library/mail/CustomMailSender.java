package com.dataart.library.mail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.apache.commons.lang.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class CustomMailSender {

	@Autowired
	private JavaMailSender mailSender;
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomMailSender.class);

	private static final String LINK = "http://localhost:8080/library/activate/?email=";

	public void activationMail(String to, String token) {
		sendMail(to, getMailParams("activation", getActivationLink(to, token)));
	}

	public void sendMail(String to, Map<String, String> mailParams) {

		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("email.html");
		String content = getString(inputStream);
		StrSubstitutor sub = new StrSubstitutor(mailParams);
		String contentToSend = sub.replace(content);
		MimeMessage mime = mailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(mime, true);
			helper.setTo(to);
			helper.setSubject(mailParams.get("subject"));
			helper.setText(contentToSend, true);

		} catch (MessagingException e) {
			e.printStackTrace();
		}
		mailSender.send(mime);

	}

	private static String getActivationLink(String email, String token) {

		return LINK + email + "&token=" + token;
	}

	public Map<String, String> getMailParams(String subject, String content) {
		Map<String, String> mailParams = new HashMap<String, String>();
		mailParams.put("subject", subject);
		mailParams.put("content", content);
		return mailParams;

	}

	private static String getString(InputStream is) {

		StringBuilder sb = new StringBuilder();
		String line;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

}
