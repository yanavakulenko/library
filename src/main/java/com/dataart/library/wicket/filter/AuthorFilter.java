package com.dataart.library.wicket.filter;

import java.io.Serializable;

public class AuthorFilter implements Serializable {
	
	public String lastName;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
