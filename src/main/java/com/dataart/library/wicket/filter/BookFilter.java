package com.dataart.library.wicket.filter;

import java.io.Serializable;

public class BookFilter implements Serializable{
	
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
