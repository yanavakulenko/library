package com.dataart.library.wicket.page;

import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.datetime.StyleDateConverter;
import org.apache.wicket.datetime.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.annotation.mount.MountPath;
import com.dataart.library.config.annotation.PagePath;
import com.dataart.library.model.Author;
import com.dataart.library.model.Book;
import com.dataart.library.service.AuthorService;
import com.dataart.library.service.BookService;
import com.dataart.library.wicket.validator.FieldsValidator;

@PagePath("/pages/books/book-edit")
@MountPath("/book")
public class BookEdit extends TemplatePage {

	private static final long serialVersionUID = 8395290529013157589L;

	@SpringBean
	private BookService bookService;

	@SpringBean
	private AuthorService authorService;

	public BookEdit(Book book) {
		add(new FeedbackPanel("feedback"));
		addForm(book);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addForm(Book book) {
		Form bookForm = new Form("bookForm", new CompoundPropertyModel(book));

		DropDownChoice authorsDropDown = new DropDownChoice<Author>("Author", new PropertyModel<Author>(book, "author"),
				new LoadableDetachableModel<List<Author>>() {

					@Override
					protected List<Author> load() {
						return authorService.findAll();
					}
				}, new ChoiceRenderer<Author>("lastName"));

		DateTextField dateTextField = new DateTextField("created", new PropertyModel<>(book, "created"),
				new StyleDateConverter("S-", true));

		DatePicker datePicker = new DatePicker();
		datePicker.setShowOnFieldClick(true);
		datePicker.setAutoHide(true);
		dateTextField.add(datePicker);

		Button submitButton = new AjaxButton("save",  new StringResourceModel("saveButton", this, null)) {

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				bookService.save(book);
			}
		};

		bookForm.add(new RequiredTextField("title").add(new FieldsValidator()));
		bookForm.add(new RequiredTextField("isbn"));
		bookForm.add(dateTextField);
		bookForm.add(authorsDropDown);
		bookForm.add(submitButton);
		add(bookForm);
	}

}
