package com.dataart.library.wicket.page;

import java.util.Map;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.EmailTextField;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.validation.EqualPasswordInputValidator;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.interpolator.MapVariableInterpolator;
import org.apache.wicket.validation.validator.EmailAddressValidator;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.wicketstuff.annotation.mount.MountPath;

import com.dataart.library.config.annotation.PagePath;
import com.dataart.library.mail.CustomMailSender;
import com.dataart.library.model.User;
import com.dataart.library.service.UserService;
import com.dataart.library.utils.Utils;

@MountPath("/register")
@PagePath("/pages/common/register")
public class Register extends WebPage {

	@SpringBean
	UserService userService;

	@SpringBean
	private PasswordEncoder passwordEncoder;

	@SpringBean
	CustomMailSender mailSender;

	private String email;

	private String password;

	private String confirmPassword;

	public Register() {

		add(new FeedbackPanel("feedback"));

		Form registerForm = new Form("registerForm");
		registerForm.setDefaultModel(new CompoundPropertyModel(this));
		registerForm.add(new EmailTextField("email").add(EmailAddressValidator.getInstance()));

		PasswordTextField userPassword = new PasswordTextField("password");
		PasswordTextField confirmPassword = new PasswordTextField("confirmPassword");
		registerForm.add(userPassword);
		registerForm.add(confirmPassword);
		
		registerForm.add(new Button("register") {

			@Override
			public void onSubmit() {
				if (userService.findOne(email)==null){
				User user = new User();
				user.setEmail(email);
				user.setPassword(passwordEncoder.encode(password));
				user.setEnabled(false);
				user.setFirstLogin(true);
				user.setCreated(Utils.getCurrentDate());
				String token = Utils.getToken();
				user.setToken(token);
				mailSender.activationMail(user.getEmail(), token);
				userService.save(user);
				}
				else {
					error("User is already registered.");
				}
			}
		});

		registerForm.add(new Link("login") {
			public void onClick() {
				setResponsePage(new LogIn());
			}
		});
		registerForm.add(new EqualPasswordInputValidator(userPassword, confirmPassword));

		add(registerForm);
	}

	

}
