package com.dataart.library.wicket.page;

import org.apache.wicket.markup.html.WebPage;
import com.dataart.library.config.annotation.PagePath;
import com.dataart.library.wicket.panel.HeaderPanel;

@PagePath("/pages/common/template-page")
public class TemplatePage extends WebPage {
	
	public TemplatePage() {
		add(new HeaderPanel("headerPanel"));
	}

}
