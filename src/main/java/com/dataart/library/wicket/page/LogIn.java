package com.dataart.library.wicket.page;

import javax.servlet.http.HttpSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.apache.wicket.request.cycle.RequestCycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wicketstuff.annotation.mount.MountPath;
import com.dataart.library.config.annotation.PagePath;

@MountPath("/login")
@PagePath("/pages/common/log-in")
public class LogIn extends WebPage {

	private final static Logger LOGGER = LoggerFactory.getLogger(LogIn.class);

	public LogIn() {

		HttpSession session = ((ServletWebRequest) RequestCycle.get().getRequest()).getContainerRequest().getSession();
		if (session.getAttribute("errorMessage") != null) {
			add(new Label("message", session.getAttribute("errorMessage").toString()));
		} else
			add(new Label("message", ""));
	}
}
