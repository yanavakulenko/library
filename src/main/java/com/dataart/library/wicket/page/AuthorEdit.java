package com.dataart.library.wicket.page;

import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.datetime.StyleDateConverter;
import org.apache.wicket.datetime.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import com.dataart.library.config.annotation.PagePath;
import com.dataart.library.model.Author;
import com.dataart.library.model.Book;
import com.dataart.library.service.AuthorService;
import com.dataart.library.service.BookService;
import com.dataart.library.wicket.panel.BookByAuthorPanel;
import com.dataart.library.wicket.panel.EmptyPanel;
import com.dataart.library.wicket.validator.FieldsValidator;

@PagePath("/pages/authors/author-edit")
public class AuthorEdit extends TemplatePage {

	private static final long serialVersionUID = 1L;

	@SpringBean
	private AuthorService authorService;

	@SpringBean
	private BookService bookService;
	
	public AuthorEdit() {
		this(null);
	}

	public AuthorEdit(Integer authorId) {
		List<Book> books = new ArrayList<>();
		
		Author author = null;
		if (authorId != null) {
			author = authorService.findOne(authorId);
			books.addAll(author.getBooks());
		} else {
			author = new Author();
		}
		
		Panel panel = (books.size() > 0) ? new BookByAuthorPanel("booksPanel", books) : new EmptyPanel("booksPanel");
		add(panel);
		add(new FeedbackPanel("feedback"));
		add(new AuthorForm("authorForm", author));
	}

	@SuppressWarnings("rawtypes")
	private class AuthorForm extends Form {

		private static final long serialVersionUID = 1L;

		@SuppressWarnings({ "unchecked" })
		public AuthorForm(String str, Author author) {
			super(str, new CompoundPropertyModel(author));

			DateTextField dateTextField = new DateTextField("dob", new PropertyModel<>(author, "dob"),
					new StyleDateConverter("S-", true));

			DatePicker datePicker = new DatePicker();
			datePicker.setShowOnFieldClick(true);
			datePicker.setAutoHide(true);
			dateTextField.add(datePicker);

			Button submitButton = new AjaxButton("save", new StringResourceModel("saveButton", this, null)) {
				@Override
				protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
					authorService.save(author);
				}
			};
			add(new RequiredTextField("firstName").add(new FieldsValidator()));
			add(new RequiredTextField("lastName").add(new FieldsValidator()));
			add(submitButton);
			add(dateTextField);
		}
	}

}
