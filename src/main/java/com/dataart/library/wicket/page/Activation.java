package com.dataart.library.wicket.page;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.StringValue;
import org.wicketstuff.annotation.mount.MountPath;

import com.dataart.library.config.annotation.PagePath;
import com.dataart.library.model.User;
import com.dataart.library.service.UserService;
import com.dataart.library.utils.Utils;

@MountPath("/activate/")
@PagePath("/pages/common/activate")
public class Activation extends TemplatePage {

	@SpringBean
	private UserService userService;

	public Activation(PageParameters parameters) {

		StringValue email = parameters.get("email");
		StringValue token = parameters.get("token");
		User user = userService.findOne(email.toString());
		if (user != null && !Utils.isExpired(user.getCreated()) && user.getFirstLogin()) {
			if (user.getToken().equals(token.toString())) {
				user.setEnabled(true);
				user.setFirstLogin(false);
				userService.save(user);
				add(new Label("message", new StringResourceModel("activationMessage", this, null)));
			}
		} else {
			add(new Label("message", "User not found or activation link is already expired."));
		}
	}

}
