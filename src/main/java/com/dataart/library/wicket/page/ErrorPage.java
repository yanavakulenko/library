package com.dataart.library.wicket.page;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.wicketstuff.annotation.mount.MountPath;

@MountPath("/error")
public class ErrorPage extends WebPage {

	public ErrorPage(String message) {
		add(new Label("message", message));
	}
}
