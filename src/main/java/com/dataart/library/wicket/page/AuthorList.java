package com.dataart.library.wicket.page;

import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import com.dataart.library.config.annotation.PagePath;
import com.dataart.library.model.Author;
import com.dataart.library.service.AuthorService;
import com.dataart.library.wicket.filter.AuthorFilter;
import com.dataart.library.wicket.provider.AuthorProvider;

@PagePath("/pages/authors/author-list")
public class AuthorList extends TemplatePage {

	@SpringBean
	private AuthorService authorService;

	private static final long serialVersionUID = 1L;

	private static final int ITEMS_PER_PAGE = 5;
	
	private DataTable<?, ?> dataTable;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public AuthorList() {

		AuthorProvider authorProvider = new AuthorProvider(authorService);
		
		dataTable = new AjaxFallbackDefaultDataTable("authorTable", addColumns(),
				authorProvider, ITEMS_PER_PAGE);
		dataTable.setOutputMarkupId(true);
		
		FilterForm<AuthorFilter> filterForm = new FilterForm<>("filterForm", authorProvider);
		TextField filterField = new TextField<>("lastName", PropertyModel.of(authorProvider, "filterState.lastName"));
		filterField.add(new OnChangeAjaxBehavior() {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				target.add(dataTable);
			}
		});
		filterForm.add(filterField);
		filterForm.add(dataTable);
		add(filterForm);

		add(new Link("newAuthor") {
			public void onClick() {
			setResponsePage(new AuthorEdit());
			}
		});
	}

	private class LinkRemovePanel extends Panel {

		private static final long serialVersionUID = 1L;

		public LinkRemovePanel(String id, IModel<Author> model) {
			super(id, model);
			add(new  AjaxFallbackLink("remove", new StringResourceModel("remove", this, null)) {
				@Override
				public void onClick (AjaxRequestTarget target) {
					Author selected = (Author) getParent().getDefaultModelObject();
					authorService.delete(selected);
					target.add(dataTable);
				}
			});
		}
	}

	private class LinkEditPanel extends Panel {

		private static final long serialVersionUID = 1L;

		public LinkEditPanel(String id, IModel<Author> model) {
			super(id, model);
			add(new  Link("edit", new StringResourceModel("edit", this, null)) {
				@Override
				public void onClick() {
					Author selected = (Author) getParent().getDefaultModelObject();
					setResponsePage(new AuthorEdit(selected.getId()));
				}
			});
		}
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	private List<IColumn> addColumns() {
		List<IColumn> columns = new ArrayList<IColumn>();
		columns.add(new PropertyColumn(new StringResourceModel("FirstName", this, null), "firstName", "firstName"));
		columns.add(new PropertyColumn(new StringResourceModel("LastName", this, null), "lastName", "lastName"));
		columns.add(new PropertyColumn(new StringResourceModel("DOB", this, null), "dob"));
		columns.add(new AbstractColumn<Author, String>(new StringResourceModel("remove", this, null)) {
			@Override
			public void populateItem(Item<ICellPopulator<Author>> cellItem, String componentId,
					IModel<Author> rowModel) {
				cellItem.add(new LinkRemovePanel(componentId, rowModel));
			}
		});

		columns.add(new AbstractColumn<Author, String>(new StringResourceModel("edit", this, null)) {
			@Override
			public void populateItem(Item<ICellPopulator<Author>> cellItem, String componentId,
					IModel<Author> rowModel) {
				cellItem.add(new LinkEditPanel(componentId, rowModel));
			}
		});
		return columns;
	}

}
