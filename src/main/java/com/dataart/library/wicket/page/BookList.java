package com.dataart.library.wicket.page;

import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.annotation.mount.MountPath;
import com.dataart.library.config.annotation.PagePath;
import com.dataart.library.model.Book;
import com.dataart.library.service.BookService;
import com.dataart.library.wicket.filter.BookFilter;
import com.dataart.library.wicket.provider.BookProvider;

@PagePath("/pages/books/book-list")
@MountPath("books")
public class BookList extends TemplatePage {

	private static final int ITEMS_PER_PAGE = 5;
	
	private DataTable<?, ?> dataTable;
	
	@SpringBean
	private BookService bookService;

	public BookList() {
		BookProvider bookProvider = new BookProvider(bookService);
		dataTable = new DefaultDataTable("bookTable", addColumns(), bookProvider, ITEMS_PER_PAGE);
		dataTable.setOutputMarkupId(true);
		
		FilterForm<BookFilter> filterForm = new FilterForm<>("filterForm", bookProvider);
		TextField filterField = new TextField<>("title", PropertyModel.of(bookProvider, "filterState.title"));
		filterField.add(new OnChangeAjaxBehavior() {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				target.add(dataTable);
			}
		});
		filterForm.add(filterField);
		filterForm.add(dataTable);

		add(filterForm);
		addNavigationLinks();
		
	}

	private List<IColumn> addColumns() {
		List<IColumn> columns = new ArrayList<IColumn>();
		columns.add(new PropertyColumn(new StringResourceModel("Title"), "title", "title"));
		columns.add(new PropertyColumn(new StringResourceModel("ISBN"), "isbn"));
		columns.add(new PropertyColumn(new StringResourceModel("Created"), "created"));
		columns.add(new PropertyColumn(new StringResourceModel("Author"), "Author.lastName"));
		columns.add(new AbstractColumn<Book, String>(new StringResourceModel("remove", this, null)) {
			@Override
			public void populateItem(Item<ICellPopulator<Book>> cellItem, String componentId, IModel<Book> rowModel) {
				cellItem.add(new LinkRemovePanel(componentId, rowModel));
			}
		});

		columns.add(new AbstractColumn<Book, String>(new StringResourceModel("edit", this, null)) {
			@Override
			public void populateItem(Item<ICellPopulator<Book>> cellItem, String componentId, IModel<Book> rowModel) {
				cellItem.add(new LinkEditPanel(componentId, rowModel));

			}
		});
		return columns;
	}

	class LinkRemovePanel extends Panel {

		private static final long serialVersionUID = 1L;

		public LinkRemovePanel(String id, IModel<Book> model) {
			super(id, model);
			add(new AjaxFallbackLink("remove", new StringResourceModel("remove", this, null)) {
				@Override
				public void onClick(AjaxRequestTarget target) {
					Book selected = (Book) getParent().getDefaultModelObject();
					bookService.delete(selected);
					target.add(dataTable);
				}
			});
		}
	}

	class LinkEditPanel extends Panel {

		private static final long serialVersionUID = 1L;

		public LinkEditPanel(String id, IModel<Book> model) {
			super(id, model);
			add(new Link("edit", new StringResourceModel("edit", this, null)) {
				@Override
				public void onClick() {
					Book selected = (Book) getParent().getDefaultModelObject();
					setResponsePage(new BookEdit(selected));
				}
			});
		}
	}

	private void addNavigationLinks() {
		add(new Link("newBook") {
			public void onClick() {
				setResponsePage(new BookEdit(new Book()));
			}
		});
	}
}
