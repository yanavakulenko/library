package com.dataart.library.wicket.panel;

import org.apache.wicket.markup.html.panel.Panel;

import com.dataart.library.config.annotation.PagePath;
@PagePath("/pages/common/empty-panel")
public class EmptyPanel extends Panel {

	public EmptyPanel(String id) {
		super(id);
	}

}
