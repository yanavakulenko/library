package com.dataart.library.wicket.panel;

import java.util.List;

import com.dataart.library.config.annotation.PagePath;
import com.dataart.library.model.Book;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;

@PagePath("/pages/common/books")
public class BookByAuthorPanel extends Panel{

	private static final int ITEMS_PER_PAGE = 5;
	
	public BookByAuthorPanel(String id, List <Book> books) {
		super(id);
		
		ListDataProvider <Book> booksProvider = new ListDataProvider<>(books);

		DataView<Book> dataView = new DataView<Book>("rows", booksProvider) {
			@SuppressWarnings("unchecked")

			@Override
			protected void populateItem(Item<Book> item) {
				Book book = item.getModelObject();
				RepeatingView repeatingView = new RepeatingView("dataRow");
				repeatingView.add(new Label(repeatingView.newChildId(), book.getTitle()));
				repeatingView.add(new Label(repeatingView.newChildId(), book.getIsbn()));
				repeatingView.add(new Label(repeatingView.newChildId(), book.getCreated()));
				item.add(repeatingView);
			}
		};
		add(new PagingNavigator("navigator", dataView));
		dataView.setItemsPerPage(ITEMS_PER_PAGE);
		add(dataView);
	}

}
