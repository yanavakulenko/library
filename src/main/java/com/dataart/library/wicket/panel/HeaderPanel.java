package com.dataart.library.wicket.panel;

import java.util.Locale;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;

import com.dataart.library.config.annotation.PagePath;
import com.dataart.library.wicket.page.AuthorList;
import com.dataart.library.wicket.page.BookList;

@PagePath("/pages/common/header-panel")
public class HeaderPanel extends Panel {

	public HeaderPanel(String id) {
		super(id);
		add(new BookmarkablePageLink("authors", AuthorList.class));
		add(new BookmarkablePageLink("books", BookList.class));

		Form languageForm = new Form("languageForm");
		languageForm.add(createLocaleChangingLink("enLink", "EN"));
		languageForm.add(createLocaleChangingLink("ruLink", "RU"));
		languageForm.add(new Link("logout") {
			public void onClick() {
				getSession().invalidate();	
			}
		});
		
		add(languageForm);
	}

	private Link createLocaleChangingLink(final String linkId, final String localeString) {
		return new Link(linkId) {
			@Override
			public void onClick() {
				changeUserLocaleTo(localeString);
			}
		};
	}

	private void changeUserLocaleTo(String localeString) {
		getSession().setLocale(new Locale(localeString));
	}

}
