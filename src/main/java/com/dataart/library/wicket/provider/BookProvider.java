package com.dataart.library.wicket.provider;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.SortOrder;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.IFilterStateLocator;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import com.dataart.library.model.Book;
import com.dataart.library.service.BookService;
import com.dataart.library.wicket.filter.BookFilter;

public class BookProvider extends SortableDataProvider<Book, String> implements IFilterStateLocator<BookFilter> {

	private BookService bookService;

	private BookFilter bookFilter = new BookFilter();

	public BookProvider(BookService bookService) {
		this.bookService = bookService;
		setSort("title", SortOrder.ASCENDING);
	}

	@Override
	public Iterator<? extends Book> iterator(long first, long count) {
		List<Book> books = filterBooks();

		Collections.sort(books, new Comparator<Book>() {

			@Override
			public int compare(Book o1, Book o2) {

				PropertyModel<Comparable> model1 = new PropertyModel<Comparable>(o1, getSort().getProperty());
				PropertyModel<Comparable> model2 = new PropertyModel<Comparable>(o2, getSort().getProperty());

				int result = model1.getObject().compareTo(model2.getObject());

				if (!getSort().isAscending()) {
					result = -result;
				}

				return result;
			}
		});
		return books.subList((int) first, (int) (first + count)).iterator();
	}

	@Override
	public long size() {

		return filterBooks().size();
	}

	private List<Book> filterBooks() {
		String title = bookFilter.getTitle();
		List<Book> books = (title != null) ? bookService.findAll(title) : bookService.findAll();
		return books;
	}

	@Override
	public IModel<Book> model(Book object) {
		return new LoadableDetachableModel<Book>() {

			@Override
			protected Book load() {

				return (Book) object;
			}
		};
	}

	@Override
	public BookFilter getFilterState() {
		return bookFilter;
	}

	@Override
	public void setFilterState(BookFilter state) {
		bookFilter = state;
	}

}
