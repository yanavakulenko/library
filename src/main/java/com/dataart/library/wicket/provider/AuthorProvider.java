package com.dataart.library.wicket.provider;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.SortOrder;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.IFilterStateLocator;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import com.dataart.library.model.Author;
import com.dataart.library.service.AuthorService;
import com.dataart.library.wicket.filter.AuthorFilter;

public class AuthorProvider extends SortableDataProvider<Author, String> implements IFilterStateLocator<AuthorFilter> {

	private static final long serialVersionUID = 1L;

	private AuthorService authorService;

	private AuthorFilter authorFilter = new AuthorFilter();

	public AuthorProvider(AuthorService authorService) {
		this.authorService = authorService;
		setSort("firstName", SortOrder.ASCENDING);
	}

	@Override
	public Iterator<Author> iterator(long first, long count) {
		List<Author> authors = filterAuthors();

		Collections.sort(authors, new Comparator<Author>() {

			@Override
			public int compare(Author o1, Author o2) {

				PropertyModel<Comparable> model1 = new PropertyModel<Comparable>(o1, getSort().getProperty());
				PropertyModel<Comparable> model2 = new PropertyModel<Comparable>(o2, getSort().getProperty());

				int result = model1.getObject().compareTo(model2.getObject());

				if (!getSort().isAscending()) {
					result = -result;
				}

				return result;
			}
		});
		return authors.subList((int) first, (int) (first + count)).iterator();
	}

	@Override
	public long size() {

		return filterAuthors().size();
	}

	@Override
	public void detach() {
	}

	@Override
	public IModel<Author> model(Author object) {
		return new LoadableDetachableModel<Author>() {

			@Override
			protected Author load() {

				return (Author) object;
			}
		};
	}

	private List<Author> filterAuthors() {
		String lastName = authorFilter.getLastName();
		List<Author> authors = (lastName != null) ? authorService.findAll(lastName) : authorService.findAll();
		return authors;
	}

	@Override
	public AuthorFilter getFilterState() {
		return authorFilter;
	}

	@Override
	public void setFilterState(AuthorFilter state) {
		authorFilter = state;
	}

}
