package com.dataart.library.wicket.validator;

import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

public class FieldsValidator implements IValidator<String> {

	private final String PATTERN = "[a-zA-Z]{2,30}";

	public FieldsValidator() {

	}

	@Override
	public void validate(IValidatable<String> validatable) {
		String text = validatable.getValue();
		if (!text.matches(PATTERN)) {
			error(validatable, "input is not valid");
		}
	}

	private void error(IValidatable<String> validatable, String errorMsg) {
		ValidationError error = new ValidationError();
		error.setMessage(errorMsg);
		validatable.error(error);
	}

}
