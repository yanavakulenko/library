package com.dataart.library.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.dataart.library.config.TestConfiguration;
import com.dataart.library.model.Author;
import com.dataart.library.model.Book;
import junit.framework.TestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfiguration.class })
public class BookDAOImplTest extends TestCase {

	@Autowired
	private BookDAO dao;
	
	@Autowired
	private AuthorDAO authorDao;

	@Test
	public void testContext() {
		assertNotNull(dao);
	}

	@Test
	public void testSave() {
		Author author = authorDao.findOne(1);
		Book book = new Book("title");
		book.setAuthor(author);

		dao.save(book);

		Assert.assertEquals(book, dao.findOne(book.getId()));
	}

	@Test
	public void testDelete() {
		int size = dao.findAll().size();
		dao.delete(dao.findOne(2));
		Assert.assertEquals(size - 1, dao.findAll().size());
	}

	@Test
	public void testFindOne() {
		Book book = dao.findOne(1);
		Assert.assertEquals("book1", book.getTitle());
	}

	@Test
	public void testFindAll() {
		Assert.assertEquals(3, dao.findAll().size());
	}

	
}
