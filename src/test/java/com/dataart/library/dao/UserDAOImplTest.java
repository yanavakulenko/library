package com.dataart.library.dao;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.dataart.library.config.TestConfiguration;
import com.dataart.library.model.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfiguration.class })
public class UserDAOImplTest {
	
	private static final String EMAIL = "test@gmail.com";
	
	private static final Logger LOGGER = LoggerFactory.getLogger("authors");

	@Autowired
	private UserDAO dao;

	@Test
	public void testContext() {
		assertNotNull(dao);
	}

	@Test
	public void testSave() {
		User user = new User("email@mail.com", "pass");
		dao.save(user);

		Assert.assertEquals(user, dao.findOne(user.getId()));
	}

	@Test
	public void testDelete() {
		int size = dao.findAll().size();

		dao.delete(dao.findOne(1));
		Assert.assertEquals(size - 1, dao.findAll().size());
	}

	@Test
	public void testFindOneById() throws ParseException {
		
		Assert.assertEquals(EMAIL, dao.findOne(1).getEmail());
	}

	@Test
	public void testFindAll() {
		
		Assert.assertEquals(2, dao.findAll().size());
	}

	@Test
	public void testFindOneByEmail() {
		
		Assert.assertEquals(EMAIL, dao.findOne(EMAIL).getEmail());
	}

}
