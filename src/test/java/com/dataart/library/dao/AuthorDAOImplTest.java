package com.dataart.library.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.dataart.library.config.TestConfiguration;
import com.dataart.library.model.Author;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfiguration.class })
public class AuthorDAOImplTest extends TestCase {

	@Autowired
	private AuthorDAO dao;

	@Test
	public void testContext() {
		assertNotNull(dao);
	}

	@Test
	public void testSave() {
		Author author = new Author("first name", "last name");
		dao.save(author);

		Assert.assertEquals(author, dao.findOne(author.getId()));
	}

	@Test
	public void testDelete() {
		int size = dao.findAll().size();
		
		dao.delete(dao.findOne(2));
		Assert.assertEquals(size - 1, dao.findAll().size());
	}

	@Test
	public void testFindOne() {
		Author author = dao.findOne(1);
		Assert.assertEquals("author1", author.getFirstName());
	}

	@Test
	public void testFindAll() {
		Assert.assertEquals(2, dao.findAll().size());
	}

}
