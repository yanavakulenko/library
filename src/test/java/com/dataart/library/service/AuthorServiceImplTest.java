package com.dataart.library.service;

import static org.mockito.Mockito.verify;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.dataart.library.dao.AuthorDAO;
import com.dataart.library.model.Author;
import com.dataart.library.service.impl.AuthorServiceImpl;
import junit.framework.TestCase;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest extends TestCase {

	@Mock
	private AuthorDAO authorDao;

	@InjectMocks
	private AuthorServiceImpl authorService;

	@Test
	public void testSave() {
		Author author = new Author("first name", "last name");

		authorService.save(author);

		verify(authorDao).save(author);
	}

	@Test
	public void testDelete() {
		authorService.delete(authorDao.findOne(1));
		verify(authorDao).delete(authorDao.findOne(1));
	}

	@Test
	public void testFindOne() {
		authorService.findOne(1);
		verify(authorDao).findOne(1);
	}

	@Test
	public void testFindAll() {
		authorService.findAll();
		verify(authorDao).findAll();
	}

	@Test
	public void testFindAllByLastName() {
		authorService.findAll("last name");
		verify(authorDao).findAll("last name");
	}

}
