package com.dataart.library.service;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.verify;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.dataart.library.dao.BookDAO;
import com.dataart.library.model.Book;
import com.dataart.library.service.impl.BookServiceImpl;
import junit.framework.TestCase;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceImplTest extends TestCase {

	@Mock
	private BookDAO bookDao;

	@InjectMocks
	private BookServiceImpl bookService;

	@Test
	public void testSave() {
		Book book = new Book("title");
		bookService.save(book);

		verify(bookDao).save(book);
	}

	@Test
	public void testDelete() {
		bookService.delete(bookDao.findOne(1));
		verify(bookDao).delete(bookDao.findOne(1));
	}

	@Test
	public void testFindOne() {
		bookService.findOne(1);
		verify(bookDao).findOne(1);
	}

	@Test
	public void testFindAll() {
		bookService.findAll();
		verify(bookDao).findAll();
	}
	
	@Test
	public void testFindAllByLastName() {
		bookService.findAll("title");
		verify(bookDao).findAll("title");
	}

}
