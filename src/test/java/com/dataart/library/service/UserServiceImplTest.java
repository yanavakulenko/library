package com.dataart.library.service;

import static org.mockito.Mockito.verify;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.dataart.library.dao.UserDAO;
import com.dataart.library.model.User;
import com.dataart.library.service.impl.UserServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {
	
	private static final String EMAIL = "test@gmail.com";
	
	@Mock
	private UserDAO dao;
	
	@InjectMocks
	private UserServiceImpl service;
		
	@Test
	public void testSave() {
		User user = new User ("email@mail.com", "123");
		service.save(user);
		verify(dao).save(user);
	}

	@Test
	public void testDelete() {
		service.delete(dao.findOne(1));
		verify(dao).delete(dao.findOne(1));
	}

	@Test
	public void testFindOneById() {
		service.findOne(1);
		verify(dao).findOne(1);
	}

	@Test
	public void testFindAll() {
		service.findAll();
		verify(dao).findAll();
	}

	@Test
	public void testFindOneByEmail() {
		service.findOne(EMAIL);
		verify(dao).findOne(EMAIL);
	}

}
