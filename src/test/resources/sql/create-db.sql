DROP TABLE IF EXISTS `author`;
CREATE TABLE `author` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `dob` date DEFAULT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `id` int(6)  NOT NULL AUTO_INCREMENT,
  `bookname` varchar(50) NOT NULL,
  `created` date DEFAULT NULL,
  `isbn` varchar(20) DEFAULT NULL,
  `author_id` int(6)  DEFAULT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(256) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `firstLogin` tinyint(1) DEFAULT NULL,
  `token` varchar(256),
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`email`)
) ;

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`role_id`,`user_id`),
);

 