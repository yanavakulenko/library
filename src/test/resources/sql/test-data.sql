INSERT INTO author VALUES (1, 'author1', 'last name', '1970-02-01');
INSERT INTO author VALUES (2, 'author2', 'last name', '1970-02-01');

INSERT INTO book  VALUES (1, 'book1',  CURRENT_TIMESTAMP(), 'isbn1', 1);
INSERT INTO book  VALUES (2, 'book2',  CURRENT_TIMESTAMP(), 'isbn2', 1);
INSERT INTO book  VALUES (3, 'book3',  CURRENT_TIMESTAMP(), 'isbn3', 2);

INSERT INTO user VALUES (1, 'test@gmail.com','123',1, CURRENT_TIMESTAMP(), NULL, 1), (2, 'email@gmail.com','123',1,CURRENT_TIMESTAMP(), NULL, 1);
INSERT INTO role VALUES(0,'ROLE_ADMIN'), (1,'ROLE_USER');			
INSERT INTO user_roles VALUES (1, 1), (2, 1);


